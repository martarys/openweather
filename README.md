# Open Weather
> These are examples of showing temperature in particular places using Python program.

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Contact](#contact)

## General info
This repository is part of my learning path in programming. Idea comes from webinar of Akademia Kodu.
Program connects with operweathermap.org using API key and get requests in json format.

## Technologies
* Python - version 3.8
* Package requests
* API

## Code Examples
import requests

page = requests.get('http://api.openweathermap.org/data/2.5/weather?q=Katowice&APPID=30c6612ef22f4b799a13fe6207d52918&&lang=pl&format=json')
print(page)
dictionary = page.json()
temperatura = dictionary['main']['temp']
temp = round(temperatura-273.15, 1)
print('Temperatura w Katowicach to ', temp, 'stopni Celsjusza')


## Status
Project is: _in progress_


## Contact
Created by [@martarys](http://pc54782.wsbpoz.solidhost.pl/wordpress/) - feel free to contact me!